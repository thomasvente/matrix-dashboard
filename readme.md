# Matrix Dashboard

Based on [allen's lab matrix-dashboard repo ](https://github.com/allenslab/matrix-dashboard)and [this awesome video overview of his creation](https://www.youtube.com/watch?v=A5A6ET64Oz8).


## Overview

- 3d print holding all the .stl files (converted from the original .step files)
- 3d print will hold the gcode files for 3d printing with my specific settings (Ender 3 Pro v2)
- code currently holding a direct copy of allen's code, will adjust / change to accommodate my needs / wishes.

